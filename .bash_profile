#
# ~/.bash_profile
#

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

# Clear offlineimap-lock
if [ -e "/var/tmp/offlineimap-lock" ]; then
	rm -f /var/tmp/offlineimap-lock
fi

# Clear proxy-flag
if [ -e "/var/tmp/proxy-mode" ]; then
	rm -f /var/tmp/proxy-mode
fi

if [ -e "~/.cargo/env" ]; then
        source $HOME/.cargo/env
        export PATH="$HOME/.cargo/bin:$PATH"
fi

# umask
umask 022
