# .bash_aliases

# Make some possibly destructive commands more interactive.
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'

# editors
alias vi='vim'
alias sbln='subl -n'
alias sbl='subl'

# grep aliases
alias egrep='egrep --color=auto'

# power aliases
alias reboot="sudo reboot"
alias poweroff="sudo poweroff"
alias halt="sudo halt"

# Path aliases
alias dev="cd ~/dev/"
alias data="cd /data/"
alias sdev="cd /srv/sdev/"
alias doc="cd ~/Documents/"
alias proj="cd ~/projects"

# Make grep more user friendly by highlighting matches
# and exclude grepping through .svn folders.
alias grep='grep -n --color=auto --exclude-dir=\.git'

#systemctl aliases
alias lds="sudo systemctl list-unit-files|grep disabled|less" #lists disabled services
alias les="sudo systemctl list-unit-files|grep enabled|less" #lists enabled services
alias lfs="sudo systemctl --failed" #lists failed services
alias findservice="sudo systemctl list-unit-files|grep -i"
#alias status="systemctl status"
#alias enable="sudo systemctl enable"
#alias disable="sudo systemctl disable"
#alias restart="sudo systemctl restart"
#alias start="sudo systemctl start"
#alias stop="sudo systemctl stop"
alias reloadconfig="sudo systemctl reload"

alias ls='ls --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias ll='ls -l --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias la='ls -la --group-directories-first --time-style=+"%d.%m.%Y %H:%M" --color=auto -F'
alias grep='grep --color=tty -d skip'
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB
alias vp='vim PKGBUILD'

alias tmutt='tsocks mutt'

alias em='emacsclient -n -c "$@"'
alias e='emacsclient -n "$@"'
alias ew='emacsclient -n -c "$@"'
alias et='emacsclient -t "$@"'
alias tup='sudo zypper dup --no-allow-vendor-change'
